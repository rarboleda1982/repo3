package com.raavsoft.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PublicprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublicprojectApplication.class, args);
	}

}
